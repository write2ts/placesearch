# PlaceSearch

This project showdcase integration with Google Map API place search and Vue.js.

## Installation

Clone the repo and use below command to install required modules.

```bash
npm install
```

Run below command to start application.

```bash
npm run serve
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)